package com.qagroup;

public class ExampleRunner {
	
	public static void main(String[] args) {
		Car car = new Car("BMW", "Black");
		car.defaultMethod();
		System.out.println(car.currentSpeed);
		//car.privateMethod();
	}

	public static void main4(String[] args) {
		int count = Car.numberOfCreatedCars();
		System.out.println(count);

		Car car1 = new Car("BMW", "White");
		Car car2 = new Car("Volvo", "Red");
		System.out.println(Car.numberOfCreatedCars());

		System.out.println(car1.numberOfCreatedCars());
		System.out.println(car2.numberOfCreatedCars());

		System.out.println(car1.getModel());
		System.out.println(car2.getModel());

	}

	public static void main2(String[] args) {
		Car car = new Car("BMW", "Black");
		car.info();

		Car car2 = new Car("Volvo", "Green");
		car2.info();

		car.info();
		String car2Color = car2.getColor();
		System.out.println(car2Color);

		car.changeColor(car2Color);
		car.info();

		car.changeColor("Red-Silver");
		car.info();
		car2.info();

	}
}
