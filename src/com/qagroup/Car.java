package com.qagroup;

public class Car {
	
	private static int counter = 0;

	private String model;

	private String color;

	int currentSpeed;
	
	public Car(String whichModel, String whichColor) {
		counter++;
		this.model = whichModel;
		this.color = whichColor;
	}
	
	public static int numberOfCreatedCars() {
		return counter;
	}
	
	public String getModel() {
		return this.model;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public void changeColor(String newColor) {
		this.color = newColor;
	}

	public void info() {
		defaultMethod();
		System.out.println("Model: " + model + "; Color: " + color + "; Current speed: " + currentSpeed);
	}
	
	void defaultMethod() {
		System.out.println("Default access method call");
		privateMethod();
	}
	
	private void privateMethod() {
		System.out.println("Private method call");
	}
}
