package com.qagroup.arrays;

public class Lec2Homework {

	public static void main(String[] args) {
		int[] arr1 = { 1, 2, 3, 4, 5 };
		Calculator calculator = new Calculator();

		int res1 = calculator.calculateProduct(arr1);
		System.out.println(res1);

		int[] arr2 = { -1, -2, -3 };
		System.out.println(calculator.calculateProduct(arr2));

		int[] arrWithZero = { 0, 1, 10, 100 };
		System.out.println(calculator.calculateProduct(arrWithZero));

		int[] emptyArr = {};
		System.out.println(emptyArr.length);
		System.out.println(calculator.calculateProduct(emptyArr));
	}

}
