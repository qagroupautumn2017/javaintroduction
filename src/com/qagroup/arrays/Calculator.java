package com.qagroup.arrays;

public class Calculator {
	public int calculateProduct(int[] array) {
		int product = 1;

		for (int element : array) {
			product = product * element;
		}

		return product;
	}
}
