package com.qagroup.test;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.qagroup.Car;

public class CarTest_Myroslav {

	@Test
	public void carCheck() {
		String expectedModel = "BMW";
		String expectedColor = "Black";

		Car car = new Car(expectedModel, expectedColor);

		String initialModel = car.getModel();
		String initialColor = car.getColor();

		Assert.assertEquals(initialModel, expectedModel);
		Assert.assertEquals(initialColor, expectedColor);

	}

	@Test
	public void changeColorCheck() {

		String Model = "Seat";
		String Color = "Blue";
		Car car = new Car(Model, Color);
		
		String newColor = "Silver";
		car.changeColor(newColor);
		String actualColor = car.getColor();

		Assert.assertEquals(actualColor, newColor);
	}

}
