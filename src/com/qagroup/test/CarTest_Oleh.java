package com.qagroup.test;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.qagroup.Car;

public class CarTest_Oleh {
	String[] ColorArr = { "white", "black", "pink", "blue" };
	String[] ModelArr = { "Bugatti", "Maserati", "Rolls-Royce" };

	Car car2 = new Car("Maserati", "pink");
	Car car3 = new Car("Rolls-Royce", "blue");

	@Test
	public void testWithColor() {
		String colorTestValue = ColorArr[1];

		Car car1 = new Car("Bugatti", colorTestValue);
		String carColor = car1.getColor();
		String expectedcolor = colorTestValue;

		Assert.assertEquals(carColor, expectedcolor);
	}

	@Test
	public void testWithColor2() {
		String carColor = car2.getColor();
		String expectedcolor = (ColorArr[1]);

		Assert.assertEquals(carColor, expectedcolor);

	}

	@Test
	public void testWithColor3() {
		String carColor = car3.getColor();
		String expectedcolor = (ColorArr[2]);

		Assert.assertEquals(carColor, expectedcolor);

	}

	@Test
	public void testWithModel1() {
		String modelTestValue = ModelArr[0];
		
		Car car1 = new Car(modelTestValue, "pink");
		String carColor = car1.getModel();
		
		String expectedModel = modelTestValue;

		Assert.assertEquals(carColor, expectedModel);

	}

	@Test
	public void testWithModel2() {
		String carColor = car2.getModel();
		String expectedcolor = (ModelArr[1]);

		Assert.assertEquals(carColor, expectedcolor);

	}

	@Test
	public void testWithModel3() {
		String carColor = car3.getModel();
		String expectedcolor = (ModelArr[2]);

		Assert.assertEquals(carColor, expectedcolor);

	}
}