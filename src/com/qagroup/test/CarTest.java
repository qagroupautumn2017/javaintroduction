package com.qagroup.test;

import java.lang.reflect.Field;

import com.qagroup.Car;

public class CarTest {

	public static void main(String[] args)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		new Car("","");
		System.out.println(Car.numberOfCreatedCars());
		
		Field studentNum = Car.class.getDeclaredField("counter");
		studentNum.setAccessible(true); // to overcome the visibility issue
		studentNum.setInt(null, 0); // null since it's static
		
		System.out.println(Car.numberOfCreatedCars());
	}

}
