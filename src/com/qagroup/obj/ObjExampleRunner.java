package com.qagroup.obj;

import com.qagroup.Car;

public class ObjExampleRunner {

	public static void main(String[] args) {
		Car car = new Car("BMW", "Black");
		//car.defaultMethod();
		
	}

	public static void main3(String[] args) {
		String str1 = new String("Hello");
		String str2 = str1.toUpperCase();
		System.out.println(str1);
		System.out.println(str2);

		new Car("", "");
		new Car("b", "c");
		new Car("bc", "de");
		System.out.println(Car.numberOfCreatedCars());
	}

	public static void main2(String[] args) {
		int a1 = 4;
		int a2 = a1;

		System.out.println("a1 = " + a1);
		System.out.println("a2 = " + a2);
		System.out.println("======");

		a2++;
		System.out.println("a1 = " + a1);
		System.out.println("a2 = " + a2);

		Car car1 = new Car("Mercedes", "Terracotta");
		Car car2 = new Car("WV", "Black");

		System.out.println("car1 color: " + car1.getColor());
		System.out.println("car2 color: " + car2.getColor());
		System.out.println("======");
		car2.changeColor("Yellow");
		System.out.println("car1 color: " + car1.getColor());
		System.out.println("car2 color: " + car2.getColor());

		Car car3 = new Car("Lada", "Green");
		Car car4 = car3;

		System.out.println("======");
		System.out.println("car3 color: " + car3.getColor());
		System.out.println("car4 color: " + car4.getColor());

		car4.changeColor("Blue");

		System.out.println("======");
		System.out.println("car3 color: " + car3.getColor());
		System.out.println("car4 color: " + car4.getColor());

		Car car5 = car4;
		car5.changeColor("White");

		System.out.println("======");
		System.out.println("car3 color: " + car3.getColor());
		System.out.println("car4 color: " + car4.getColor());

	}
}
